#JWT / RSA on PHP  Proof Of Concept
 ---
 
##Disclaimer

By no means should this code ever be used in production ready applications nor be executed on production servers. No security checks and/or validations were enforced. This code was written for educational purposes only, having the scope to showcase basic functionality. Performance, efficiency, security, or reusability were not a priority.

##Install

Run `composer install` to install all the library dependencies.
Run `php vendor/vwit/foundation/copy2assets.php public/assets`  from CLI if needed to copy the assets

