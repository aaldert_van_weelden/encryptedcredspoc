<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Two Step</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">
        
         <script type="text/javascript">
			var ASSET_URL = 'assets';
		</script>
	 	
		<script type="text/javascript"src="assets/js_lib/jquery/jquery-1.11.3.min.js"></script>
	    <script type="text/javascript"src="assets/js_lib/utils/util.js"></script>
	    <script type="text/javascript"src="assets/js_lib/utils/request.js"></script>
	    <script type="text/javascript"src="assets/js_lib/utils/events.js"></script>
	    <script type="text/javascript"src="assets/js_lib/utils/md5.js"></script>
		<script type="text/javascript" src="assets/js_lib/utils/encrypt.js" ></script>
		
		<script type="text/javascript" src="assets/js_lib/knockout/knockout-3.0.0.js" ></script>

        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Encrypted Credentials 2Step Proof Of Concept</a>
          <a class="navbar-brand pull-right" href="#">Login : demo/test</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
         </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <p>Submit the form and send the RSA encrypted credentials.</p>
      </div>
    </div>

    <div class="container">

		<form class="form" method="post" action="xhr/formactie.php" id="loginform">
			<fieldset>
				<legend>RSA encrypt 2Step</legend>
	            
	                <fieldset>
	                <h4>Safe fields to be encrypted</h4>
	                
	                <div class="form-group">
					    <label for="username">Gebruikersnaam</label>
					    <input class="form-control" name="username" type="text" data-bind="value:username"/>
					</div>
					
					<div class="form-group">
					    <label for="userpass">Wachtwoord</label>
					    <input class="form-control" name="userpass" type="text" data-bind="value:userpass"/>
					</div>
					
	                </fieldset>
	                <br><br>
	                <fieldset>
			                <h4>Regular fields, no encryption</h4>
			                
			                <div class="form-group">
							    <label for="city">City</label>
							    <input class="form-control" name="city" type="text" value="Arnhem"/>
							</div>
							
							<div class="form-group">
							    <label for="email">Email</label>
							    <input class="form-control" name="email" type="text" value="mail@test.com"/>
							</div>
							
							<div class="form-group">
							    <label for="dummy">Dummy field</label>
							    <input class="form-control" name="dummy" type="text" value="Excluded from submission"/>
							</div>
	              
	                    </fieldset>
	                    <br><br>
	                    <fieldset>
								<button type="submit" class="btn btn-default" data-bind="click:encryptAndSubmit">Encrypt and submit the form</button>
	                        </li>
	                       
					
	                </fieldset>
	
			</fieldset>
	
	  
		</form>
	
	<br><br><br>
	      <footer>
	        <p>Van Weelden &copy; 2016</p>
	      </footer>
    </div>
    
    <script src="js/vendor/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/vendor/jsrsasign-6.2.0-all-min.js"></script>
	<script type="text/javascript" src="js/rsa.2step.login.js" ></script>
	<script type="text/javascript" src="js/index.js" ></script>
    
    </body>
</html>