/**
 * 2-step login
 */
var RsaLogin = (function() {

	var LOGIN_FORM = 'loginform',
		USERNAMEFIELD = 'username',
		PASSWORDFIELD = 'userpass';
	
	var interface = {

		init : function() {
			presenter.init();
		}
	};

	var viewModel = {

		username : ko.observable('demo'),
		userpass : ko.observable('test'),

		encryptAndSubmit : function() {
			console.log('Submit button clicked');
			presenter.encryptAndSubmit();
		}
	};

	function View(model) {

		this.model = model;
		this.loginForm = $('#'+LOGIN_FORM);
		this.bindReady = false;

		var self = this;

		this.init = function() {
			ko.applyBindings(self.model, self.loginForm[0]);
			self.bindReady = true;
		};
	}

	var view = {};

	var presenter = {
		
		jwt : '',

		/**
		 * The RSA options
		 * @see assets/js_lib/utils/encrypt.js
		 */
		options : {
			form 		: LOGIN_FORM,
			keyurl 		: 'auth_jwt/key',
			posturl 	: 'auth_jwt/rsa',
			encodeURI 	: 'base64safe'//encode all characters except [A-Z],[a-z] and[0-9]
		},

		init : function() {

			//set the configured RSA options
			$.serialize.setOptions(this.options);

			//get the JSON Web Token providing the RSA keys in callback function   
			$.rsaGetJwtKey(function(jwt) {
				presenter.jwt = jwt;
				view = new View(viewModel);
				view.init();

			});
		},

		/**
		 * Handle form submission:
		 * Serialize form and encrypt the fields-to-encrypt
		 */
		encryptAndSubmit : function() {
			console.log('Trying to submit the credentials...');

			$.serialize.andEncryptBeforeSubmit({
				fieldsToEncrypt : [ USERNAMEFIELD, PASSWORDFIELD ],
				beforeSubmit : function(obj) {
					console.log('pass the partial encrypted formdata');
					console.dir(obj);
					presenter.checkCredentials(obj);
				},
				noSubmit : {
					//do not submit this form elements
					"submitButton" : false,
					"dummy" : false
				},
			});
		},

		/**
		 * Send the JWT-signed RSA encrypted username and password and
		 * check the credentials
		 */
		checkCredentials : function(obj) {

			$.ajax({
				method : obj.type,
				url : obj.url,
				beforeSend: function(request){
					request.setRequestHeader('Authorization', 'Bearer ' + presenter.jwt);
		        },
				data : obj.data
			}).done(function(response) {
				console.dir(response);
				if(response.data && $.parseBoolean(response.data.authenticated)){
					alert('U bent succesvol ingelogd');
					console.log('TODO: redirect to the user homepage');
					//TODO redirect the user here
				}else{
					alert('Uw gebruikersnaam en/of wachtwoord wordt niet herkend');
				}
				
			}).fail(function(error) {
				console.error(error.statusText);
				alert('Fout: ' + (error.statusText=='Unauthorized'?'Uw gebruikersnaam en/of wachtwoord wordt niet herkend':'Er is een onverwachte fout gedetecteerd') );
			});
		}


	};

	return interface;

})();
