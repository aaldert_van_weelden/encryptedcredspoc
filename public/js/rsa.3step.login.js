/**
 * Legacy 3-step login
 */
var RsaLogin = (function() {

	var LOGIN_FORM = 'loginform';
	var interface = {

		init : function() {
			presenter.init();
		}
	};

	var viewModel = {

		username : ko.observable('demo'),
		userpass : ko.observable('test'),

		encryptAndSubmit : function() {
			presenter.encryptAndSubmit();
		}
	};

	function View(model) {

		this.model = model;
		this.loginForm = $('#'+LOGIN_FORM);
		this.bindReady = false;

		var self = this;

		this.init = function() {
			ko.applyBindings(self.model, self.loginForm[0]);
			self.bindReady = true;
		};
	}

	var view = {};

	var JWT = {

		jwt   : {},
		claim : {},

		/**
		 * Decodes the JWT
		 * @param jwt
		 * @returns {*}
		 */
		decodeToken : function(jwt) {
			var a = jwt.split(".");
			return b64utos(a[1]);
		},

		/**
		 * Sets the JWT to the store object
		 * @param data
		 */
		setJwt : function(data) {
			this.jwt = data;
			this.claim = this.decodeToken(data);
		},

		getJwt : function() {
			return this.jwt;
		},

		/**
		 * Retrieve the JWT claim
		 * @returns {*}
		 */
		getClaim : function() {
			return JSON.parse(this.claim);
		}

	};

	var presenter = {

		/**
		 * The RSA options
		 * @see assets/js_lib/utils/encrypt.js
		 */
		options : {
			form 		: LOGIN_FORM,
			keyurl 		: 'auth/key',
			posturl 	: 'auth/rsa',
			checkurl 	: 'auth/check',
			encodeURI 	: 'base64safe'//encode all characters except [A-Z],[a-z] and[0-9]
		},

		init : function() {

			//set the RSA options
			$.serialize.setOptions(this.options);

			//get RSA server key and pass onsuccess callback function   
			$.rsaGetKey(function(keys) {

				view = new View(viewModel);
				view.init();

			});
		},

		/**
		 * Serialize form and encrypt the fields-to-encrypt
		 */
		encryptAndSubmit : function() {
			console.log('Trying to submit the credentials...');

			$.serialize.andEncryptBeforeSubmit({
				fieldsToEncrypt : [ 'username', 'userpass' ],
				beforeSubmit : function(obj) {
					//pass the partial encrypted formdata
					console.dir(obj);
					presenter.checkUser(obj);
				},
				noSubmit : {
					//do not submit this form elements
					"submitButton" : false,
					"dummy" : false
				},
			});
		},

		/**
		 * Send the RSA encrypted username and password
		 * Check the username and retrieve the token
		 */
		checkUser : function(obj) {

			$.ajax({
				method : obj.type,
				url : obj.url,
				data : obj.data
			}).done(function(data) {
				console.dir(data);
				JWT.setJwt(data.jwt);
				var claim = JWT.getClaim();
				console.dir(claim);
				presenter.checkPassword(claim.data);
			}).fail(function(error) {
				console.error(error.statusText);
				alert('Fout: ' + (error.statusText=='Unauthorized'?'Uw gebruikersnaam en/of wachtwoord wordt niet herkend':'Er is een onverwachte fout gedetecteerd') );
			});
		},

		/**
		 * Check the credentials against the authentication service and check the JWT signed saltedchallenge
		 */
		checkPassword : function(data) {

			var saltedChallenge = $.hex_md5($.hex_md5(data.salt
					+ view.model.userpass())
					+ data.challenge);

			submitData = {
				saltedchallenge : saltedChallenge
			};
			
			$.ajax({
				method : "POST",
				url : presenter.options.checkurl,
				data : submitData,
				beforeSend: function(request){
					request.setRequestHeader('Authorization', 'Bearer ' + JWT.getJwt());
		        },
			}).done(function(data) {
				console.dir(data);

				if (!$.parseBoolean(data.authenticated) || data.authenticated == "geen rechten") {
					
					if (data.authenticated == "geen rechten") {
						alert('U heeft geen (actieve) rechten binnen deze applicatie');
					} else {
						alert('Uw gebruikersnaam en/of wachtwoord wordt niet herkend');
					}
				} else {
					console.log('trying to reload the page for the authenticated user...');
					alert('Successfully logged in!');
					// location.reload();
				}
			}).fail(function(error) {
				console.error(error.statusText);
				alert('Fout: ' + (error.statusText=='Unauthorized'?'Uw gebruikersnaam en/of wachtwoord wordt niet herkend':'Er is een onverwachte fout gedetecteerd') );
			});
		}

	};

	return interface;

})();
