<?php
chdir ( dirname ( __DIR__ ) );
require_once ('../vendor/autoload.php');

session_start ();

use Firebase\JWT\JWT;
use Utils\Util;
use ThirdParty\JCryption;

/**
 * Stubbed authentication service
 */
class AuthService{
	
	/**
	 * In real worl send credentials encrypted over the wire (ESB)
	 * @param string $username
	 * @param string $userpass
	 * @return boolean
	 */
	 public function authenticate($username, $userpass){
	 	return false;
	 }
}

/**
 * Stubbed User model
 * @author Aaldert
 *
 */
class DB{
	
	public static function User(){
		$user = new stdClass ();
		$user->id = 1001;
		$user->username = 'demo';
		$user->salt = '345623458';
		$user->password = md5($user->salt.'test');
		return $user;
	}
}
/**
 * Dummy login controller emulation, legacy to support CodeIgniter login procedure
 */
class LoginController {
	
	private $authService;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		//stub service
		$this->authService = new AuthService();
	}
	
	/**
	 * Route emulation
	 * Handle the requests to this controller.
	 * To emulate routing the request parameter is set in the htaccess file
	 */
	public function handleRequest() {
		$method = $_SERVER ['REQUEST_METHOD'];
		
		switch ($method) {
			
			case 'GET' :
				
				/**
				 * @url public/auth/key
				 * @method GET
				 */
				if (isset ( $_GET ['route'] ) && $_GET ['route'] == 'key') {
					$this->getKeys ();
				}
				break;
			
			case 'POST' :
				
				/**
				 * @url public/auth/rsa
				 * @method POST
				 */
				if (isset ( $_GET ['route'] ) && $_GET ['route'] == 'submit') {
					$data = file_get_contents ( "php://input" );
					$this->checkUsername ( $data );
				}
				
				/**
				 * @url public/auth/check
				 * @method POST
				 */
				if (isset ( $_GET ['route'] ) && $_GET ['route'] == 'check') {
					$authHeader = getallheaders () ['Authorization'];
					$saltedchallenge = $_POST['saltedchallenge'];
					$this->checkUserPassword ($authHeader, $saltedchallenge);
				}
				
				break;
		}
	}
	
	/**
	 * Retrieve the generated RSA keys
	 */
	public function getKeys() {
		$keyLength = 1024;
		$jCryption = new JCryption ();
		
		$keys = $jCryption->generateKeypair ( $keyLength );
		
		$_SESSION ["e"] = array (
				"int" => $keys ["e"],
				"hex" => $jCryption->dec2string ( $keys ["e"], 16 ) 
		);
		$_SESSION ["d"] = array (
				"int" => $keys ["d"],
				"hex" => $jCryption->dec2string ( $keys ["d"], 16 ) 
		);
		$_SESSION ["n"] = array (
				"int" => $keys ["n"],
				"hex" => $jCryption->dec2string ( $keys ["n"], 16 ) 
		);
		
		$keys = [
			'e' => $_SESSION ["e"] ["hex"],
			'n' => $_SESSION ["n"] ["hex"],
			'maxdigits' => intval ( $keyLength * 2 / 16 + 3 )
		];
		header ( 'Content-type: application/json' );
		echo json_encode ( $keys );
	}
	
	/**
	 * Check if the provided username exists and return the JWT
	 * @param array $data  The encrypted username and password sent by the client
	 * @response The JWT
	 */
	public function checkUsername($data) {
		$jCryption = new JCryption ();
		
		$sJson = Util::uriToJSONstring ( $data, 'base64safe' ); // JSON string
		$oJson = json_decode ( $sJson ); // php object
		
		$var = $jCryption->decrypt ( $oJson->safe, $_SESSION ["d"] ["int"], $_SESSION ["n"] ["int"] );
		
		parse_str ( $var, $vars );
		
		$vars = ( object ) $vars;
		
		// use keys only once
		unset ( $_SESSION ["e"] );
		unset ( $_SESSION ["d"] );
		unset ( $_SESSION ["n"] );
		
		if ( ! (property_exists ( $vars, 'userpass' ) && Util::notNullOrEmpty ( $vars->userpass ) ) ) {
			header ( 'HTTP/1.0 401 Unauthorized' );
			return false;
		}
		
		if (property_exists ( $vars, 'username' ) && Util::notNullOrEmpty ( $vars->username )) {
			try {
				
				//Stubbed.  In real world retrieve the user from the database to validate the username
				$user = DB::User();
				
				$challenge = Util::random_string ( 10 );
				
				$_SESSION ['challenge'] = $challenge;
				$_SESSION ['userdata'] = $user;
				$_SESSION ['userpass'] = $vars->userpass;//store for check against external portal service
				
				if ($vars->username === $user->username) {
					
					$data = [
							'userid' => $user->id, // userid from the users table
							'salt' => $user->salt, // salt from the users table
							'challenge' => $challenge 
					]
;
					
					$secretKey = base64_encode ( '1234' );
					
					$jwt = JwtHelper::create ( $data, $secretKey );
					
					$unencodedArray = [ 
							'jwt' => $jwt 
					];
					
					header ( 'Content-type: application/json' );
					echo json_encode ( $unencodedArray );
				} else {
					header ( 'HTTP/1.0 401 Unauthorized' );
				}
			} catch ( Exception $e ) {
				print $e->__toString();//not in real world
				header ( 'HTTP/1.0 500 Internal Server Error' );
			}
		} else {
			header ( 'HTTP/1.0 400 Bad Request' );
		}
	}
	
	/**
	 * Check if the JWT header token is valid and check if the provided salted challenge is valid for login
	 * @param string $authHeader
	 * @response JSON 
	 */
	public function checkUserPassword($authHeader, $saltedchallenge) {

		if ($authHeader) {
			/*
			 * Extract the jwt from the Bearer
			 */
			list ( $jwt ) = sscanf ( $authHeader, 'Bearer %s' );
			
			if ($jwt) {
				try {
					
					$secretKey = '1234';
					
					// adjust for minor timeshift in WAMP develop
					JWT::$leeway = 10;
					
					$token = JWT::decode ( $jwt, $secretKey, [ 
							JwtHelper::HS512 
					] );
					
					// JWT token is valid
					//retrieve the properties from the session
					$challenge = $_SESSION ['challenge'];
					$user = $_SESSION ['userdata'];//in real world use the id to fetch the user from the DB
					$userpass = $_SESSION ['userpass'];//in real world check this against external auth service (ESB)
					
					$isAuthenticated = false;
					
					//stubbed service
					if($this->authService->authenticate($user->username, $userpass)){
						
						$isAuthenticated = true;
						
					}else{
						//user not present in external auth service, check if the user is authorized in the scope of the application
						$tmpuserid = $user->id;	
						$DBpassword = $user->password;
						$DBsalt = $user->salt;
						
						$challengedww = md5 ( $DBpassword . $challenge );
	
						if (($challengedww !== $saltedchallenge)) {
							$isAuthenticated = false;
						} else {
							// login user here
							$isAuthenticated = true;
						}
					}
					
					header ( 'Content-Type: application/json' );
					echo json_encode ( [ 
							'authenticated' => $isAuthenticated 
					] );
					
				} catch ( Exception $e ) {
					//token could not be decoded, probably tempered with
					print $e->__toString ();//not in real world
					header ( 'HTTP/1.0 401 Unauthorized' );
				}
			} else {
				//jwt token could not be extracted
				header ( 'HTTP/1.0 400 Bad Request' );
			}
		} else {
			//The request lacks the authorization token
			print 'Token not found in request';//not in real world
			header ( 'HTTP/1.0 400 Bad Request' );
			
		}
	}
}

$login = new LoginController ();
$login->handleRequest ();