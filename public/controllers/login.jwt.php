<?php
chdir ( dirname ( __DIR__ ) );
require_once ('../vendor/autoload.php');

session_start ();

use Firebase\JWT\JWT;
use Utils\Util;
use ThirdParty\JCryption;
use ThirdParty\JCryptKeyDTO;
use XHR\SimpleResponse;

/**
 * Stubbed authentication service
 */
class AuthService{
	
	/**
	 * In real worl send credentials encrypted over the wire (ESB)
	 * @param string $username
	 * @param string $userpass
	 * @return boolean
	 */
	 public function authenticate($username, $userpass){
	 	return false;
	 }
}

/**
 * Stubbed User model
 * @author Aaldert
 *
 */
class DB{
	
	public static function User(){
		$user = new stdClass ();
		$user->id = 1001;
		$user->username = 'demo';
		$user->salt = '345623458';
		$user->password = md5($user->salt.'test');
		return $user;
	}
}
/**
 * Dummy login controller emulation. Used as 2step login procedure in ELW
 */
class LoginJwtController {
	
	const JCRYPT = 'JCRYPT';
	
	private $authService;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		//stub service
		$this->authService = new AuthService();
	}
	
	/**
	 * Route emulation
	 * Handle the requests to this controller.
	 * To emulate routing the request parameter is set in the htaccess file
	 */
	public function handleRequest() {
		$method = $_SERVER ['REQUEST_METHOD'];
		
		switch ($method) {
			
			case 'GET' :
				
				/**
				 * @url public/auth/key
				 * @method GET
				 */
				if (isset ( $_GET ['route'] ) && $_GET ['route'] == 'key') {
					$this->getKeys ();
				}
				break;
			
			case 'POST' :
				
				/**
				 * @url public/auth/rsa
				 * @method POST
				 */
				if (isset ( $_GET ['route'] ) && $_GET ['route'] == 'submit') {
					$authHeader = getallheaders () ['Authorization'];
					$data = file_get_contents ( "php://input" );
					$this->checkCredentials ($authHeader, $data, $_SESSION[self::JCRYPT] );
					unset ( $_SESSION[self::JCRYPT] );//use keys only once
				}
				
				break;
		}
	}
	
	/**
	 * Retrieve the generated RSA keys
	 */
	public function getKeys() {
		
		$dto = new JCryptKeyDTO();
		
		$_SESSION[self::JCRYPT] = $dto;
	
		$secretKey = base64_encode ( '1234' );//TODO: in real world retrieve from config file
			
		$jwt = JwtHelper::create ( $dto->publickeys, $secretKey );
		
		$response = new SimpleResponse('JWT');
		$response->setData($jwt);
		$response->sendJSON();
		
		
	}
	
	/**
	 * Check if the provided username exists and return the JWT
	 * @param string $authHeader  The authentication header
	 * @param array $data  The encrypted username and password sent by the client
	 * @param JCryptKeyDTO $dto   The dto object with the RSA keys generated for the current request exclusively
	 * @response The JWT
	 */
	public function checkCredentials($authHeader, $data, JCryptKeyDTO $dto) {
		
		if ($authHeader) {
			/*
			 * Extract the jwt from the Bearer
			 */
			list ( $jwt ) = sscanf ( $authHeader, 'Bearer %s' );
				
			if ($jwt) {
				try {
						
					$secretKey = '1234';//TODO: in real world retrieve from config file
						
					// adjust for minor timeshift in WAMP develop
					JWT::$leeway = 10;
						
					$token = JWT::decode ( $jwt, $secretKey, [
							JwtHelper::HS512
					] );
					//JWT is valid
					
					$jCryption = new JCryption ();
					
					$sJson = Util::uriToJSONstring ( $data, 'base64safe' ); // JSON string
					$oJson = json_decode ( $sJson ); // php object
					
					$var = $jCryption->decrypt ( $oJson->safe, $dto->d->int, $dto->n->int );
					
					parse_str ( $var, $vars );
					
					$vars = ( object ) $vars;
					
					$isAuthenticated = false;
					
					if ( ! (property_exists ( $vars, 'userpass' ) && Util::notNullOrEmpty ( $vars->userpass ) ) ) {
						header ( 'HTTP/1.0 401 Unauthorized' );
						return false;
					}
					
					if (property_exists ( $vars, 'username' ) && Util::notNullOrEmpty ( $vars->username )) {
						try {
					
							//TODO: Stubbed.  In real world retrieve the user from the database to validate the username
							$user = DB::User();
							
							if ($vars->username === $user->username) {
								
								//check password here locally
								if($user->password === md5($user->salt.$vars->userpass) ){
									$isAuthenticated = true;
								}else{
									//on failure: check password by  authentication service
									$isAuthenticated = $this->authService->authenticate($user->username, $vars->userpass);
								}
								
								$response = new SimpleResponse('AUTHENTICATION');
								$response->setData( ['authenticated'=>$isAuthenticated] );
								$response->sendJSON();
								
							} else {
								header ( 'HTTP/1.0 401 Unauthorized' );
							}
						} catch ( Exception $e ) {
							print $e->__toString();//not in real world
							header ( 'HTTP/1.0 500 Internal Server Error' );
						}
					} else {
						header ( 'HTTP/1.0 400 Bad Request' );
					}
						
				
						
				} catch ( Exception $e ) {
					//token could not be decoded, probably tempered with
					print $e->__toString ();//not in real world
					header ( 'HTTP/1.0 401 Unauthorized' );
				}
			} else {
				//jwt token could not be extracted
				header ( 'HTTP/1.0 400 Bad Request' );
			}
		} else {
			//The request lacks the authorization token
			print 'Token not found in request';//not in real world
			header ( 'HTTP/1.0 400 Bad Request' );
				
		}
			
	}
	
}

$login = new LoginJwtController ();
$login->handleRequest ();